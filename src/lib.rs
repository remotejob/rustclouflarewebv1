use serde_json::json;
use worker::*;

mod utils;

fn log_request(req: &Request) {
    console_log!(
        "{} - [{}], located at: {:?}, within: {}",
        Date::now().to_string(),
        req.path(),
        req.cf().coordinates().unwrap_or_default(),
        req.cf().region().unwrap_or("unknown region".into())
    );
}

#[event(fetch,respond_with_errors)]
pub async fn main(req: Request, env: Env, ctx: worker::Context) -> Result<Response> {
    log_request(&req);

    // Optionally, get more helpful error messages written to the console in the case of a panic.
    utils::set_panic_hook();



    let mycountry = req.headers().get("cf-ipcountry");


    let router = Router::new();
    router
    .get("/tst", |_req, ctx| {
        Response::ok(ctx.var("WORKERS_RS_VERSION")?.to_string())
    })
    .or_else_any_method_async("/*catchall", |_, ctx| async move {
        console_log!(
            "[or_else_any_method_async] caught: {}",
            ctx.param("catchall").unwrap_or(&"?".to_string())
        );

        Fetch::Url("https://github.com/404".parse().unwrap())
            .send()
            .await
            .map(|resp| resp.with_status(404))
    })
    .run(req, env).await


    


    // match mycountry {
    //     Ok(country) => {

    //         let unwrapcountry = country.unwrap();

    //         if &unwrapcountry == "FI" {

    //             let router = Router::new();




    //     //         router
    //     //         .get_async("/", |_req, ctx| async move {
    //     //             let kv = ctx.kv("rustclouflareweb-index_html")?;
    //     //             let (value, _metadata) = kv
    //     //                 .get("index.html")
    //     //                 .bytes_with_metadata::<Vec<usize>>()
    //     //                 .await?;
    //     //             let resstr = value.unwrap_or_default();
    //     //             let mut response = Response::from_bytes(resstr)?;
        
    //     //             let headers = response.headers_mut();
        
    //     //             headers.set("Content-Type", "text/html; charset=UTF-8")?;
        
    //     //             Ok(response)
        
    //     //             // Response::from_html(resstr.to_string())
    //     //         })

    //     //         .run(req, env)
    //     // .await



    //         } else if &unwrapcountry == "US" {
    //             let useragent = req.headers().get("User-Agent");
    //             match useragent {
    //                 Ok(useragent) => {

    //                     if useragent.unwrap().contains("oogle") {
    //                         // console_log!("{:?}", "OK google");

    //                         let mut headers = Headers::new();
    //                         headers
    //                             .set(
    //                                 "User-Agent",
    //                                 "Googlebot/2.1 (+http://www.google.com/bot.html)",
    //                             )
    //                             .unwrap();

    //                         let mut init = RequestInit::new();
    //                         init.method = Method::Get;
    //                         init.headers = headers;

    //                         let resp = Fetch::Request(Request::new_with_init(
    //                             "https://hoivataan.fi/test.html",
    //                             &init,
    //                         )?)
    //                         .send()
    //                         .await?
    //                         .text()
    //                         .await?;

    //                         return Response::from_html(resp);
    //                     }
    //                 }

    //                 Err(_some_error) => println!("{}", "error"),
    //             }
    //         }
           
    //     }
    //     Err(_some_error) => println!("{}", "error"),
    // }


    

    // Optionally, use the Router to handle matching endpoints, use ":name" placeholders, or "*name"
    // catch-alls to match on specific patterns. Alternatively, use `Router::with_data(D)` to
    // provide arbitrary data that will be accessible in each route via the `ctx.data()` method.
    // let router = Router::new();

    // // Add as many routes as your Worker needs! Each route will get a `Request` for handling HTTP
    // // functionality and a `RouteContext` which you can use to  and get route parameters and
    // // Environment bindings like KV Stores, Durable Objects, Secrets, and Variables.
    // router
    //     .get("/", |_, _| Response::ok("Hello from Workers!"))
    //     .post_async("/form/:field", |mut req, ctx| async move {
    //         if let Some(name) = ctx.param("field") {
    //             let form = req.form_data().await?;
    //             match form.get(name) {
    //                 Some(FormEntry::Field(value)) => {
    //                     return Response::from_json(&json!({ name: value }))
    //                 }
    //                 Some(FormEntry::File(_)) => {
    //                     return Response::error("`field` param in form shouldn't be a File", 422);
    //                 }(req, env).await
    //     })
    //     .get("/worker-version", |_, ctx| {
    //         let version = ctx.var("WORKERS_RS_VERSION")?.to_string();
    //         Response::ok(version)
    //     })
    //     .run(req, env)
    //     .await


    // Response::error("Bad Request", 400)
}
